/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include "SettingsDaemon.hpp"

int main( int argc, char *argv[] ) {

	QCoreApplication app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "DesQ Settings Daemon" );
    app.setApplicationVersion( "1.0.0" );

    QCommandLineParser parser;
    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

	parser.addOption( { { "d", "daemon" }, "Run this application as a daemon", "" } );
	parser.addOption( { "no-daemon", "Don't run this application as a daemon", "" } );

    parser.process( app );

    /*
        *
        * The reference used for writing this code into a daemon.
        * http://www.netzmafia.de/skripten/unix/linux-daemon-howto.html
        *
    */

    if ( not parser.isSet( "no-daemon" ) ) {

        pid_t pid = fork();

        if ( pid < 0 ) {

            // Something went wrong
            qDebug() << "Unable to create daemon process";

            // Exit the program
            exit( EXIT_FAILURE );
        }

        if ( pid > 0 ) {

            // Parent process. Inform the user and retire.
            qDebug() << "desq-settings-daemon will continue running in the background.";

            // Exit the parent
            exit( EXIT_SUCCESS );
        }

        /* Change the file mode mask */
        umask( 0 );

        /* Create a new SID for the child process */
        pid_t sid = setsid();
        if ( sid < 0 ) {
            // TODO: Write to log file
            exit( EXIT_FAILURE );
        }

        /* Change to home folder */
        if ( chdir( QDir::homePath().toUtf8().data() ) < 0 ) {
            // TODO: Write to error log
            exit( EXIT_FAILURE );
        }

        /* Close the std file descriptors */
        close( STDIN_FILENO );
        close( STDOUT_FILENO );
        close( STDERR_FILENO );
    }

	/* Our actual code starts here */

	DesQSettingsDaemon d( "DesQ" );

    /* Some one else has taken this service!! Grrr... */
	if ( not d.isServiceRunning() ) {
		qDebug() << "Unable to start the service 'org.DesQ.Settings'.";
        qDebug() << "Please ensure that another instance of this app is not running elsewhere.";
		return 1;
	}

    /* Something went wrong... */
	else if ( not d.isObjectRegistered() ) {
		qDebug() << "Unable to register the object '/org/DesQ/Settings'.";
        qDebug() << "Please ensure that another instance of this app is not running elsewhere.";
		return 1;
	}

    /* All is well. */
	else {
		qDebug() << "Service org.DesQ.Settings running...";
		qDebug() << "Object /org/DesQ/Settings registered...";
		qDebug() << "Ready for incoming connections...";
	}

    return app.exec();
};
