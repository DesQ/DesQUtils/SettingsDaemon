/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "SettingsDaemon.hpp"
#include "SettingsAdaptor.hpp"
#include <desq-config.h>

static inline QVariantMap readUserSettings( QString proj, QString app ) {

	QVariantMap map;
	QSettings sett( proj, app );
	for( QString key: sett.allKeys() ) {
		map[ key ] = sett.value( key );
	}

	return map;
}

static inline QVariant variantMapToVariant( QVariantMap valueMap ) {

	QVariant value = valueMap[ "value" ];
	int type = (int)valueMap[ "type" ].toInt();

	QDBusArgument dbusArg = value.value<QDBusArgument>();
	switch( type ) {
		case QMetaType::QStringList: {
			QStringList list;
			dbusArg >> list;
			return list;
		}

		case QMetaType::QRect: {
			QRect rect;
			dbusArg >> rect;
			return rect;
		}

		case QMetaType::QRectF: {
			QRectF rect;
			dbusArg >> rect;
			return rect;
		}

		case QMetaType::QSize: {
			QSize size;
			dbusArg >> size;
			return size;
		}

		case QMetaType::QSizeF: {
			QSizeF size;
			dbusArg >> size;
			return size;
		}

		default: {
			qDebug() << "SettingsDaemon" << valueMap[ "key" ] << valueMap[ "value" ] << type;
			qDebug() << "SettingsDaemon" << "Unknown QDbusArgumentType" << dbusArg.currentType();
			return QVariant();
		}
	}

	return QVariant();
};

DesQSettingsDaemon::DesQSettingsDaemon( QString project ) : QObject(), mProject( project ) {

	new SettingsAdaptor( this );

	QDBusConnection dbus = QDBusConnection::sessionBus();
    objectRegistered = dbus.registerObject( "/org/DesQ/Settings", this );
    serviceRunning = dbus.registerService( "org.DesQ.Settings" );

    QDBusInterface session( "org.DesQ.Session", "/org/DesQ/Session", "org.DesQ.Session", dbus );
    session.call( "RegisterService", "org.DesQ.Settings" );

	confWatcher = new QFileSystemWatcher( this );
	connect( confWatcher, &QFileSystemWatcher::fileChanged, this, &DesQSettingsDaemon::handleFileChanged );
};

bool DesQSettingsDaemon::isServiceRunning() {

	return serviceRunning;
};

bool DesQSettingsDaemon::isObjectRegistered() {

	return objectRegistered;
};

void DesQSettingsDaemon::reload() {

	for( QString app: mUserSettingsMap.keys() )
		mUserSettingsMap[ app ]->sync();
};

QVariantMap DesQSettingsDaemon::value( QString app, QString key ) {

	if ( not mUserSettingsMap.contains( app ) ) {
		mUserSettingsMap[ app ] = new QSettings( mProject, app );
		confWatcher->addPath( mUserSettingsMap[ app ]->fileName() );
		fileAppMap[ mUserSettingsMap[ app ]->fileName() ] = app;
		mOldSettingsData[ app ] = readUserSettings( mProject, app );

		if ( QString( ConfigPath ).startsWith( "/" ) )
			mDefSettingsMap[ app ] = new QSettings( QString( ConfigPath ) + mProject + app + ".conf", QSettings::IniFormat );

		else
			mDefSettingsMap[ app ] = new QSettings( QString( InstallPrefix ) + ConfigPath + mProject + app + ".conf", QSettings::IniFormat );
	}

	mUserSettingsMap[ app ]->sync();
	QVariant value;

	/* Here we can define some specialized/dynamic settings: Example form factor */
	if ( key == "TouchMode" ) {
		int touch = mUserSettingsMap[ app ]->value( key, mDefSettingsMap[ app ]->value( key ) ).toInt();
		switch ( touch ) {
			/* Auto detect */
			case 0: {
				QProcess proc;
				proc.start( "udevadm", QStringList() << "info" << "--export-db" );
				proc.waitForFinished( -1 );

				QString output = QString::fromLocal8Bit( proc.readAllStandardOutput() + '\n' + proc.readAllStandardError() );
				value = ( output.contains( "ID_INPUT_TOUCHSCREEN=1" ) ? true : false );
				break;
			}

			/* TouchMode is on */
			case 1:
				value = QVariant( true );
				break;

			/* TouchMode is off */
			case 2:
				value = QVariant( false );
				break;

			default:
				value = QVariant( false );
				break;
        }
	}

	else if ( key == "TouchModeRaw" ) {

		value = mUserSettingsMap[ app ]->value( "TouchMode", mDefSettingsMap[ app ]->value( key ) );
	}

	/* Get the default value for the key */
	else {
		QVariant defaultValue = mDefSettingsMap[ app ]->value( key );
		value = mUserSettingsMap[ app ]->value( key, defaultValue );

		if ( (QMetaType::Type)value.type() == QMetaType::QString ) {
			QString strValue = value.toString();
			if ( strValue.startsWith( "~" ) )
				strValue = strValue.replace( "~", QDir::homePath() );

			value = QVariant( strValue );
		}

		else if ( (QMetaType::Type)value.type() == QMetaType::QFont ) {
			QFont f = value.value<QFont>();
			value = QString( "QFont\n%1\n%2\n%3\n%4" ).arg( f.family() ).arg( f.pointSize() ).arg( f.weight() ).arg( f.italic() );
		}
	}

	QVariantMap map;
	map[ "value" ] = value;
	map[ "type" ] = (int)value.type();

	return map;
};

void DesQSettingsDaemon::setValue( QVariantMap valueMap ) {

	QString app = valueMap[ "app" ].toString();
	QString key = valueMap[ "key" ].toString();
	QVariant value = valueMap[ "value" ];

	if ( not mUserSettingsMap.contains( app ) ) {
		mUserSettingsMap[ app ] = new QSettings( mProject, app );
		mDefSettingsMap[ app ] = new QSettings( QString( ":/Settings/%1%2.conf" ).arg( mProject ).arg( app ), QSettings::NativeFormat );
	}

	if ( value.type() >= 1024 )
		value = variantMapToVariant( valueMap );

	if ( (QMetaType::Type)value.type() == QMetaType::QString ) {
		QString str = value.toString();
		if ( str.startsWith( "QFont\n" ) ) {
			QStringList font = str.split( "\n" );
			QString name = font.at( 1 );
			int pointSize = font.at( 2 ).toInt();
			int weight = font.at( 3 ).toInt();
			bool italic = ( font.at( 4 ) == "true" ? true : false );

			value = QVariant( QFont( name, pointSize, weight, italic ) );
		}
	}

	mUserSettingsMap[ app ]->setValue( key, value );
	mUserSettingsMap[ app ]->sync();

	emit settingChanged( valueMap );
};

void DesQSettingsDaemon::handleFileChanged( QString file ) {

	/* To insulate ourselves against write-new-file-and-delete-old method */
	if ( not confWatcher->files().contains( file ) and QFile::exists( file ) )
		confWatcher->addPath( file );

	/* We need to compare the older values to the new values and emit the correct signal */
	QString app = fileAppMap[ file ];
	mUserSettingsMap[ app ]->sync();

	QVariantMap newAppData = readUserSettings( mProject, app );
	for( QString key: newAppData.keys() ) {

		QVariantMap map;
		map[ "app" ] = app;
		map[ "key" ] = key;
		map[ "value" ] = newAppData[ key ];
		map[ "type" ] = (int)newAppData[ key ].type();

		if ( not mOldSettingsData.contains( key ) ) {
			emit settingChanged( map );
			continue;
		}

		if ( mOldSettingsData[ app ].value( key ) != newAppData.value( key ) ) {
			emit settingChanged( map );
			continue;
		}
	}

	/* Refresh @mOldSettingsData with the current data */
	mOldSettingsData[ app ] = newAppData;
};
