/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>
#include <QtDBus>
#include <qcontainerfwd.h>

class SettingsAdaptor: public QDBusAbstractAdaptor {

    Q_OBJECT;

    Q_CLASSINFO( "D-Bus Interface", "org.DesQ.Settings" );
    Q_CLASSINFO( "D-Bus Introspection", ""
        "<interface name=\"org.DesQ.Settings\">\n"
        "   <signal name=\"settingChanged\">\n"
        "       <arg direction=\"out\" type=\"a{sv}\" name=\"value\"/>\n"
        "       <annotation value=\"QVariantMap\" name=\"org.qtproject.QtDBus.QtTypeName.Out0\"/>\n"
        "   </signal>\n"
        "   <method name=\"reload\"/>\n"
        "   <method name=\"value\">\n"
        "       <arg direction=\"out\" type=\"a{sv}\"/>\n"
        "       <annotation value=\"QVariantMap\" name=\"org.qtproject.QtDBus.QtTypeName.Out0\"/>\n"
        "       <arg direction=\"in\" type=\"s\" name=\"app\"/>\n"
        "       <arg direction=\"in\" type=\"s\" name=\"key\"/>\n"
        "   </method>\n"
        "   <method name=\"setValue\">\n"
        "       <arg direction=\"in\" type=\"a{sv}\" name=\"value\"/>\n"
        "       <annotation value=\"QVariantMap\" name=\"org.qtproject.QtDBus.QtTypeName.In0\"/>\n"
        "   </method>\n"
        "</interface>\n"
    "");

    public:
        SettingsAdaptor(QObject *parent);
        virtual ~SettingsAdaptor();

    public Q_SLOTS:
        void reload();
        void setValue( const QVariantMap &value );
        QVariantMap value( const QString &app, const QString &key );

    Q_SIGNALS:
        void settingChanged( const QVariantMap &value );
};
